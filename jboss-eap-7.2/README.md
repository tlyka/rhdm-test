#### Red Hat Decision Central

**How to Integrate RHDM with an external Git Repository**

By default the Decision Central doesn't support integrating projects with external repositories.
In order for the users to be able to save their new projects externally or update the existing ones, you have to implement a post-commit hook.

Every time a user makes a change in Decision Central, a post-commit hook is triggered in order to update the external repos with the latest changes.
You have to create a "hooks" directory inside JBoss with the post-commit file and give the right permissions. The contents of the post-commit file is the following:
```
#!/bin/sh
git push origin master
```
This post-commit file allows projects that have been imported through Github to automatically push all the changes in RHDM to its original external Git repo.


To solve the problem of creating new projects in RHDM and automatically create an external Git repo, we have added a custom solution.
The code of the Java integration code can be found here: https://bitbucket.org/aviosgroup/rhdm-external-github-integration
In order to use this integration, clone this repository and build it locally. Then copy the JAR to your local hooks directory inside JBoss and change the post-commit file to the following:
```
#!/bin/sh
java -jar ./hooks/git-push-2.1-SNAPSHOT.jar
```

If you need to change the directory of the hooks folder, simple change the path in the standalone.xml file:

```<property name="org.uberfire.nio.git.hooks" value="../hooks"/>```

If you want to change the location of the JAR, change the path inside the post-commit file.

The Java application uses a configuration file .gitremote that is located under the user home directory. If you run first the application, it will automatically create a new file in the specified directory that you can modify.

***Example of .gitremote***

```
#This is an auto generated template empty property file
provider=GIT_HUB
login=
password=
token=
remoteGitUrl=https://api.github.com/
useSSH=false
ignore=.*demo.*, test.*
githubOrg=OrgName
gitlabGroup=Group/subgroup
```

***Parameters:***
- **provider:** This is the flavor of the Git provider, currently only two values are accepted: GIT_HUB and GIT_LAB. Mandatory.
- **login:** username. Mandatory.
- **password:** plain text password. Not mandatory if token is provided.
- **token:** this is the generated token to replace username/password unsecure connection, if this is not set you will get a warning that you are using an unsecured connection. Not mandatory if password is provided.
- **remoteGitUrl:** This can be either a public provider URL or locally hosted enterprise for any provider. Mandatory.
- **useSSH:** use the SSH protocol to push changes to the remote repository. Optional, default = false. Note: this config uses BC local ~/.ssh/ directory to obtain SSH config.
- **ignore:** This is a comma separated regular expressions to ignore the project names that matches any of these expressions. Optional.
- **githubOrg:** If GitHub is used as provider, it's possible to define the repository organization in this property. Optional.
- **gitlabGroup:** If GitLab is used as provider, it's possible to define the group/subgroup in this property. Optional.

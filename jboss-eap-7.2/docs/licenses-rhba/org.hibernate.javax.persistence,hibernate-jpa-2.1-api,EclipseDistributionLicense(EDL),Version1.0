<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Mike Milinkovich, January 25, 2008"/>
    <meta name="keywords" content="edl, legal, license"/>
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,700,300,600,100" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="/eclipse.org-common/themes/solstice/public/images/favicon.ico"/>
    <title>Eclipse Distribution License</title>
    <link rel="stylesheet" href="/eclipse.org-common/themes/solstice/public/stylesheets/styles.min.css"/>
<meta property="og:description" content="Eclipse is probably best known as a Java IDE, but it is more: it is an IDE framework, a tools framework, an open source project, a community, an eco-system, and a foundation."/>
<meta property="og:image" content="https://www.eclipse.org/eclipse.org-common/themes/solstice/public/images/logo/eclipse-200x200.png"/>
<meta property="og:title" content="Eclipse Distribution License"/>
<meta property="og:image:width" content="200"/>
<meta property="og:image:height" content="200"/>
<meta itemprop="name" content="Eclipse Distribution License"/>
<meta itemprop="description" content="Eclipse is probably best known as a Java IDE, but it is more: it is an IDE framework, a tools framework, an open source project, a community, an eco-system, and a foundation."/>
<meta itemprop="image" content="https://www.eclipse.org/eclipse.org-common/themes/solstice/public/images/logo/eclipse-400x400.png"/>
<meta name="twitter:site" content="@EclipseFdn"/>
<meta name="twitter:card" content="summary"/>
<meta name="twitter:title" content="Eclipse Distribution License"/>
<meta name="twitter:url" content="http://www.eclipse.org/org/documents/edl-v10.php"/>
<meta name="twitter:description" content="Eclipse is probably best known as a Java IDE, but it is more: it is an IDE framework, a tools framework, an open source project, a community, an eco-system, and a foundation."/>
<meta name="twitter:image" content="https://www.eclipse.org/eclipse.org-common/themes/solstice/public/images/logo/eclipse-400x400.png"/>

    <script> var eclipse_org_common = {"settings":{"cookies_class":{"name":"eclipse_settings","enabled":1}}}</script>  </head>
  <body id="body_solstice">
    <a class="sr-only" href="#content">Skip to main content</a>
        <div class="clearfix toolbar-container-wrapper">
      <div class="container">
        <div class="text-right toolbar-row row hidden-print">
          <div class="col-md-24 row-toolbar-col">
            <ul class="list-inline">
              <li><a href="https://accounts.eclipse.org/user/register"><i class="fa fa-user fa-fw"></i> Create account</a></li>
              <li><a href="https://accounts.eclipse.org/user/login/?takemeback=http%3A%2F%2Fwww.eclipse.org%2Forg%2Fdocuments%2Fedl-v10.php"><i class="fa fa-sign-in fa-fw"></i> Log in</a></li>
              
            </ul>
          </div>
        </div>
      </div>
    </div><header role="banner" id="header-wrapper">
  <div class="container">
    <div class="row" id="header-row">
            <div class="hidden-xs col-sm-8 col-md-6 col-lg-5" id="header-left">
        <div class="wrapper-logo-default"><a href="http://www.eclipse.org/"><img class="logo-eclipse-default img-responsive hidden-xs" alt="logo" src="//www.eclipse.org/eclipse.org-common/themes/solstice/public/images/logo/eclipse-426x100.png"/></a></div>
      </div>            <div class="col-sm-10 col-md-8 col-lg-5 hidden-print hidden-xs pull-right" id="header-right">
            <div class="row"><div class="col-md-24">
    <div id="custom-search-form" class="reset-box-sizing">
    <script>
      (function() {
        var cx = '011805775785170369411:p3ec0igo0qq';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//cse.google.com/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
      })();
    </script>
    <gcse:searchbox-only gname="main" resultsUrl="https://www.eclipse.org/home/search.php"></gcse:searchbox-only>
    </div></div></div>
        <div id="btn-call-for-action"><a href="//www.eclipse.org/downloads/" class="btn btn-huge btn-warning"><i class="fa fa-download"></i> Download</a></div>
      </div>      <div class="col-sm-14 col-md-16 col-lg-19 reset" id="main-menu-wrapper">
  <div class="navbar yamm" id="main-menu">
    <div id="navbar-collapse-1" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li class="visible-thin"><a href="http://www.eclipse.org/downloads/" target="_self">Download</a></li><li><a href="http://www.eclipse.org/users/" target="_self">Getting Started</a></li><li><a href="http://www.eclipse.org/membership/" target="_self">Members</a></li><li><a href="http://www.eclipse.org/projects/" target="_self">Projects</a></li>                  <li class="dropdown visible-xs"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Community <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://marketplace.eclipse.org">Marketplace</a></li><li><a href="http://events.eclipse.org">Events</a></li><li><a href="http://www.planeteclipse.org/">Planet Eclipse</a></li><li><a href="http://www.eclipse.org/community/eclipse_newsletter/">Newsletter</a></li><li><a href="https://www.youtube.com/user/EclipseFdn">Videos</a></li></ul></li><li class="dropdown visible-xs"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Participate <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="https://bugs.eclipse.org/bugs/">Report a Bug</a></li><li><a href="http://www.eclipse.org/forums/">Forums</a></li><li><a href="http://www.eclipse.org/mail/">Mailing Lists</a></li><li><a href="https://wiki.eclipse.org/">Wiki</a></li><li><a href="https://wiki.eclipse.org/IRC">IRC</a></li><li><a href="http://www.eclipse.org/contribute/">How to Contribute</a></li></ul></li><li class="dropdown visible-xs"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Working Groups <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://iot.eclipse.org">Internet of Things</a></li><li><a href="http://locationtech.org">LocationTech</a></li><li><a href="http://lts.eclipse.org">Long-Term Support</a></li><li><a href="http://polarsys.org">PolarSys</a></li><li><a href="http://science.eclipse.org">Science</a></li><li><a href="http://www.openmdm.org">OpenMDM</a></li></ul></li>          <!-- More -->
          <li class="dropdown eclipse-more hidden-xs">
            <a data-toggle="dropdown" class="dropdown-toggle">More<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li>
                <!-- Content container to add padding -->
                <div class="yamm-content">
                  <div class="row">
                    <ul class="col-sm-8 list-unstyled"><li><p><strong>Community</strong></p></li><li><a href="http://marketplace.eclipse.org">Marketplace</a></li><li><a href="http://events.eclipse.org">Events</a></li><li><a href="http://www.planeteclipse.org/">Planet Eclipse</a></li><li><a href="http://www.eclipse.org/community/eclipse_newsletter/">Newsletter</a></li><li><a href="https://www.youtube.com/user/EclipseFdn">Videos</a></li></ul><ul class="col-sm-8 list-unstyled"><li><p><strong>Participate</strong></p></li><li><a href="https://bugs.eclipse.org/bugs/">Report a Bug</a></li><li><a href="http://www.eclipse.org/forums/">Forums</a></li><li><a href="http://www.eclipse.org/mail/">Mailing Lists</a></li><li><a href="https://wiki.eclipse.org/">Wiki</a></li><li><a href="https://wiki.eclipse.org/IRC">IRC</a></li><li><a href="http://www.eclipse.org/contribute/">How to Contribute</a></li></ul><ul class="col-sm-8 list-unstyled"><li><p><strong>Working Groups</strong></p></li><li><a href="http://iot.eclipse.org">Internet of Things</a></li><li><a href="http://locationtech.org">LocationTech</a></li><li><a href="http://lts.eclipse.org">Long-Term Support</a></li><li><a href="http://polarsys.org">PolarSys</a></li><li><a href="http://science.eclipse.org">Science</a></li><li><a href="http://www.openmdm.org">OpenMDM</a></li></ul>                  </div>
                </div>
              </li>
            </ul>
          </li>
              </ul>
    </div>
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </button>
      <div class="wrapper-logo-mobile"><a class="navbar-brand visible-xs" href="http://www.eclipse.org/"><img class="logo-eclipse-default-mobile img-responsive" alt="logo" src="//www.eclipse.org/eclipse.org-common/themes/solstice/public/images/logo/eclipse-800x188.png"/></a></div>    </div>
  </div>
</div>
    </div>
  </div>
</header>
    <section class="hidden-print default-breadcrumbs" id="breadcrumb">
      <div class="container">
        <h3 class="sr-only">Breadcrumbs</h3>
        <div class="row">
          <div class="col-sm-16 padding-left-30"><ol class="breadcrumb"><li><a href="http://www.eclipse.org/">Home</a></li><li><a href="http://www.eclipse.org/org">About Us</a></li><li><a href="http://www.eclipse.org/org/documents">Governance Documents</a></li><li class="active">Eclipse Distribution License</li></ol></div>
          <div class="col-sm-8 margin-top-15"></div>
        </div>
        
      </div>
    </section> <!-- /#breadcrumb --><main class="no-promo">
  <div class="novaContent container" id="novaContent">
              <!-- nav -->
  <aside id="leftcol" class="col-md-4">
        <ul id="leftnav" class="ul-left-nav fa-ul hidden-print">
      
        
                      <li class="separator">
              <a class="separator" href="/org/">
                About Us              </a>
            </li>
          
              
        
                      <li>
              <i class="fa fa-angle-double-right orange fa-fw"></i>
              <a href="/org/foundation/reports/annual_report.php" target="_self">
                Annual Report              </a>
            </li>
          
              
        
                      <li>
              <i class="fa fa-angle-double-right orange fa-fw"></i>
              <a href="/org/foundation/" target="_self">
                Foundation              </a>
            </li>
          
              
        
                      <li>
              <i class="fa fa-angle-double-right orange fa-fw"></i>
              <a href="/org/documents/" target="_self">
                Governance              </a>
            </li>
          
              
        
                      <li>
              <i class="fa fa-angle-double-right orange fa-fw"></i>
              <a href="/legal/" target="_self">
                Legal Resources              </a>
            </li>
          
              
        
                      <li>
              <i class="fa fa-angle-double-right orange fa-fw"></i>
              <a href="/org/foundation/contact.php" target="_self">
                Contact Us              </a>
            </li>
          
                  </ul>
      </aside>
            
	<div id="midcolumn">
		<h1>Eclipse Distribution License  - v 1.0</h1>

<p>Copyright (c) 2007, Eclipse Foundation, Inc. and its licensors. </p>

<p>All rights reserved.</p>
<p>Redistribution and use in source and binary forms, with or without modification, 
	are permitted provided that the following conditions are met:</p>
<ul><li>Redistributions of source code must retain the above copyright notice, 
	this list of conditions and the following disclaimer. </li>
<li>Redistributions in binary form must reproduce the above copyright notice, 
	this list of conditions and the following disclaimer in the documentation 
	and/or other materials provided with the distribution. </li>
<li>Neither the name of the Eclipse Foundation, Inc. nor the names of its 
	contributors may be used to endorse or promote products derived from 
	this software without specific prior written permission. </li></ul>

<p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.</p>
</div>
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>OSI Approved</h6>The Eclipse Distribution License is an OSI Approved Open Source License by means of the 
			<a href="http://www.opensource.org/licenses/BSD-3-Clause" target="blank">New BSD License</a>.
			<p align="center"><a href="http://www.opensource.org/licenses/BSD-3-Clause" target="blank"><img align="center" src="../../images/osi-certified-60x50.gif" border="0"/>&nbsp</a></p>
			<h6>Purpose</h6>
			Use of the Eclipse Distribution License by any project at the Eclipse Foundation must 
			be reviewed and unanimously approved by the <a href="../foundation/directors.php">Board of Directors</a>.
			<h6>Related Links</h6>
			<ul>
			<li>The standard Eclipse license is the <a href="epl-v10.php">Eclipse Public License</a>.</li>
			<li><a href="edl-v10.html">EDL in plain HTML</a></li>
			</ul>
		</div>
	</div>
  </div>
</main> <!-- /#main-content-container-row --><p id="back-to-top">
  <a class="visible-xs" href="#top">Back to the top</a>
</p>
<footer role="contentinfo" id="solstice-footer">
  <div class="container">
    <div class="row">
      <section class="col-sm-offset-1 col-xs-11 col-sm-7 col-md-6 col-md-offset-0 hidden-print" id="footer-eclipse-foundation">
            <h2 class="section-title">Eclipse Foundation</h2>
    <ul class="nav">
    <li><a href="http://www.eclipse.org/org/">About us</a></li>
    <li><a href="http://www.eclipse.org/org/foundation/contact.php">Contact Us</a></li>
    <li><a href="http://www.eclipse.org/donate">Donate</a></li>
      <li><a href="http://www.eclipse.org/org/documents/">Governance</a></li>
      <li><a href="http://www.eclipse.org/artwork/">Logo and Artwork</a></li>
      <li><a href="http://www.eclipse.org/org/foundation/directors.php">Board of Directors</a></li>
    </ul>      </section>
      <section class="col-sm-offset-1 col-xs-11 col-sm-7 col-md-6 col-md-offset-0 hidden-print" id="footer-legal">
            <h2 class="section-title">Legal</h2>
    <ul class="nav">
      <li><a href="http://www.eclipse.org/legal/privacy.php">Privacy Policy</a></li>
      <li><a href="http://www.eclipse.org/legal/termsofuse.php">Terms of Use</a></li>
      <li><a href="http://www.eclipse.org/legal/copyright.php">Copyright Agent</a></li>
      <li><a href="http://www.eclipse.org/org/documents/epl-v10.php">Eclipse Public License </a></li>
      <li><a href="http://www.eclipse.org/legal/">Legal Resources </a></li>
    </ul>      </section>
      <section class="col-sm-offset-1 col-xs-11 col-sm-7 col-md-6 col-md-offset-0 hidden-print" id="footer-useful-links">
            <h2 class="section-title">Useful Links</h2>
    <ul class="nav">
      <li><a href="https://bugs.eclipse.org/bugs/">Report a Bug</a></li>
      <li><a href="//help.eclipse.org/">Documentation</a></li>
      <li><a href="http://www.eclipse.org/contribute/">How to Contribute</a></li>
      <li><a href="http://www.eclipse.org/mail/">Mailing Lists</a></li>
      <li><a href="http://www.eclipse.org/forums/">Forums</a></li>
      <li><a href="//marketplace.eclipse.org">Marketplace</a></li>
    </ul>      </section>
      <section class="col-sm-offset-1 col-xs-11 col-sm-7 col-md-6 col-md-offset-0 hidden-print" id="footer-other">
            <h2 class="section-title">Other</h2>
    <ul class="nav">
      <li><a href="http://www.eclipse.org/ide/">IDE and Tools</a></li>
      <li><a href="http://www.eclipse.org/projects">Community of Projects</a></li>
      <li><a href="http://www.eclipse.org/org/workinggroups/">Working Groups</a></li>
      <li><a href="http://www.eclipse.org/org/research/">Research@Eclipse</a></li>
    </ul>

    <ul class="list-inline social-media">
      <li><a href="https://twitter.com/EclipseFdn"><i class="fa fa-twitter-square"></i></a></li>
      <li><a href="https://plus.google.com/+Eclipse"><i class="fa fa-google-plus-square"></i></a></li>
      <li><a href="https://www.facebook.com/eclipse.org"><i class="fa fa-facebook-square"></i> </a></li>
      <li><a href="https://www.youtube.com/user/EclipseFdn"><i class="fa fa-youtube-square"></i></a></li>
      <li><a href="https://www.linkedin.com/company/eclipse-foundation"><i class="fa fa-linkedin-square"></i></a></li>
    </ul>      </section>
            <div id="copyright" class="col-sm-offset-1 col-sm-14 col-md-24 col-md-offset-0">
        <span class="hidden-print"><div class="wrapper-logo-eclipse-white"><a href="http://www.eclipse.org"><img class="logo-eclipse-white img-responsive" alt="logo" src="//www.eclipse.org/eclipse.org-common/themes/solstice/public/images/logo/eclipse-logo-bw-332x78.png"/></a></div></span>
        <p id="copyright-text">Copyright &copy; 2017 The Eclipse Foundation. All Rights Reserved.</p>
      </div>      <a href="#" class="scrollup">Back to the top</a>
    </div>
  </div>
</footer>
<!-- Placed at the end of the document so the pages load faster -->
<script src="/eclipse.org-common/themes/solstice/public/javascript/main.min.js"></script>

      <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-910670-2']);
        _gaq.push(['_trackPageview']);

        (function() {
          var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

      </script>
</body>
</html>

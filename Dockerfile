FROM openjdk:8-jre-alpine

ENV M2_HOME=/repositories/kie/global

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh

COPY jboss-eap-7.2 /opt/jboss-eap-7.2

ADD .gitremote /root/.gitremote

EXPOSE 8080

CMD /opt/jboss-eap-7.2/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0